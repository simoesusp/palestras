# Palestras

- Prof. Eduardo do Valle Simões 	   		
- Grupo de Sistemas Embarcados e Evolutivos
- LCR – Laboratório de Computação Reconfigurável
- Departamento de Sistemas de Computação
- Instituto de Ciências Matemáticas e de Computação - USP

## Palestra de Hoje (15/05/2024)

- https://docs.google.com/presentation/d/1yt7rh_xz9CPfSIrS3Ebu8gvbwagyEbmfG1Km47K2J5M/edit?usp=sharing

## Curso sobre Inteligência Artificial

- Os Slides utilizados estão disponíeis em: https://gitlab.com/simoesusp/palestras/-/tree/main/Slides?ref_type=heads

- Aula 01 - Foi uma discussão sobre o tema, foi transmitida mas nao foi gravada!!

- Aula 02 - https://drive.google.com/file/d/1lZfYBakQ8O49QNtRH9XC-FumAuK91Nlk/view?usp=sharing

- Aula 03 - https://drive.google.com/file/d/1B3F0K4Z4ArbKCuEM6RbFBB3patnhDI6j/view?usp=sharing

- Aula 04 - https://drive.google.com/file/d/1TtlQwimYP16nhxu9u81t6POP6vEhswFr/view?usp=sharing

- Aula 05 - https://drive.google.com/file/d/15WOZRbYHQhVlR9yq6I7vD-xQ1xNCpXgD/view?usp=sharing

- Aula 06 - Não foi gravada!!



## Link para os Videos sobre os experimentos:

- https://drive.google.com/drive/folders/1KUCdakEi15zEm7RI0A0VRtokHPR5EeXd?usp=sharing

## Eu Robo - An Imitation of Life

- https://youtu.be/gjr-enQdVOw?si=C3XcjNBV_stHKYGQ&t=74

## Herika - Skyrim Chat GPT

- https://youtu.be/0svu8WBzeQM?si=xSl_dnUK7VYgS1MD&t=102

## Ants
- https://drive.google.com/file/d/1Czn_vDEZqt4Okc224eNcHgG3Gd2mMM11/view?usp=sharing

## Workshop Robotica

- https://gitlab.com/simoesusp/workshop-robotica

## Contatos:

- www.icmc.usp.br/~simoes/
- Link do Projeto Formiguinhas: https://github.com/matheuslosilva/Hardware-Accelerated-Ant-Colony-Based-Swarm-System
- Link para os slides e vídeos: shorturl.at/kuHR2
- Link para o Projeto das Abelhinhas: https://github.com/brenocq/Honeybee-Simulation
- Link para Disciplina de Sistemas Evolutivos: https://gitlab.com/simoesusp/disciplinas/-/tree/master/SSC0713-Sistemas-Evolutivos-Aplicados-a-Robotica/Software?ref_type=heads
- Github/Gitlab:simoesusp                    
- email: simoes@icmc.usp.br

